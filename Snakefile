SAMPLES, = glob_wildcards("/g/scb2/zeller/SHARED/DATA/metaG/SIMULATED/metaPhlAn2/segata/{samples}.fastq.bz2")



rule all:
    input:
        expand("/home/holze/euk_motus_test/mpa2_results/bowtie/{samples}", samples = SAMPLES),
        expand("/home/holze/euk_motus_test/mpa2_results/{samples}.mpa2",   samples = SAMPLES)


rule do_run_mpa2:
    input:
        "/g/scb2/zeller/SHARED/DATA/metaG/SIMULATED/metaPhlAn2/segata/{samples}.fastq.bz2"
    output:
        alignments = "/home/holze/euk_motus_test/mpa2_results/bowtie/{samples}.bowtie2.bz2",
        profile    = "/home/holze/euk_motus_test/mpa2_results/{samples}.mpa2"
    threads:
        8
    message:
        "run MetaPlAn2"
    conda:
        "envs/mpa2.yaml"
    shell:
        """ metaphlan2.py {input} --bowtie2out {output.alignments} \
             --input_type fastq --nproc {threads} -o {output.profile} """

